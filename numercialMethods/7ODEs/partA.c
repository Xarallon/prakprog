#include "ode.h"

int main(int argc, char const *argv[]){
	
	printf("Test working \n");

	FILE *mystream = fopen("log","w");

	
    gsl_vector * yt = gsl_vector_alloc(2);
    gsl_vector_set(yt,0,1);
    gsl_vector_set(yt,1,0);
	
	double t = 0;
	double h = 0.01;

	// *t, b, h, vec yt, acc, eps, stepper, f, log file
	driver(&t,20*M_PI,h, yt, 1e-6,1e-6,rkstep12,harm_osc,mystream);


	gsl_vector_free(yt);
	fclose(mystream);

	return 0;
}