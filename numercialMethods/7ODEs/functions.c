#include "ode.h"

void orbit(double t, gsl_vector * y, gsl_vector * dydt){
	
	gsl_vector_set(dydt,0,
		gsl_vector_get(y,1));

	gsl_vector_set(dydt,1,   1.0 - gsl_vector_get(y,0)
		-0.01*gsl_vector_get(y,0)*gsl_vector_get(y,0));
}

void harm_osc(double x, gsl_vector * y, gsl_vector * dydx){
	
	gsl_vector_set(dydx,0,gsl_vector_get(y,1));
	gsl_vector_set(dydx,1,-gsl_vector_get(y,0));
}