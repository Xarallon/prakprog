#include "ode.h"

void rkstep12(
	double t, 			// Current value of the variable
	double h, 			// the step to be taken
	gsl_vector * yt,	// the current value the sought function
	void f(double t, gsl_vector * yt, gsl_vector * dydt), 
						// Right hand side of dydt = f(t,y)
	gsl_vector * yth,	// output: y(t+h)
	gsl_vector * err 	// output: error estimates
	){

	int n = yt->size;
	gsl_vector * k0 = gsl_vector_alloc(n);
	gsl_vector * y_temp = gsl_vector_alloc(n);
	gsl_vector * k12 = gsl_vector_alloc(n);

	f(t, yt, k0);

	for (int i = 0; i < n; ++i)	{
		gsl_vector_set(y_temp, i,
			gsl_vector_get(yt,i) + gsl_vector_get(k0,i) *h/2 );
	}

	f(t+h/2, y_temp, k12);

	for (int i = 0; i < n; ++i) {
		gsl_vector_set(yth, i,
			gsl_vector_get(yt,i) + gsl_vector_get(k12,i) *h );
	}

	// error calcs
	// optimistic
	for (int i = 0; i < n; ++i) {
		gsl_vector_set(err, i,
			(gsl_vector_get(k0,i) - gsl_vector_get(k12,i)) *h/2 );
	}

	


	gsl_vector_free(k0);
	gsl_vector_free(k12);
	gsl_vector_free(y_temp);
}