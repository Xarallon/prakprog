#include "odetwo.h"

int main(int argc, char const *argv[]){
	
	printf("Test working \n");

	FILE *mystream = fopen("log","w");

	int n = 2;

	
    gsl_vector * yt = gsl_vector_alloc(n);
    gsl_vector * yp = gsl_vector_alloc(n);
    gsl_vector_set(yt,0,1);
    gsl_vector_set(yt,1,0);
	
	double t = 0, tp;
	double h = 1e-3;
	double b = 20*M_PI;

	double eps = 1e-6, acc = 1e-6;


	// Make first step
	double E, y_norm, tau, a = t;
	gsl_vector * yth = gsl_vector_alloc(n);
	gsl_vector * err = gsl_vector_alloc(n);
	
	int stepppp = 0;
	int firstStepUntaken = 1;
	while (firstStepUntaken) {		
		
		// call rkstep12
		rkstep12(t,h,yt,orbit,yth,err);

		//	Check if step is within tolorance tau
		E 	   = gsl_blas_dnrm2(err);
		y_norm = gsl_blas_dnrm2(yth);
		tau = (y_norm * eps + acc) * sqrt(h/(b-a));


		if (E < tau) { // Take first step
			tp = t;
			t += h;
			gsl_vector_memcpy(yp,yt);
			gsl_vector_memcpy(yt,yth);
			firstStepUntaken = 0;
		}

		// Correct the step
		if (E > 0){
			h *= pow(tau/E, 0.25) * 0.95;
		}
		else {
			h *=2;
		}
		stepppp++;

	}		
	fprintf(stderr, "%d\n",stepppp );


	// *t, tp, b, h, vec yp, vec yt, acc, eps, stepper, f, log file
	driver_twoStep(&t,tp,b,h, yp, yt, acc, eps,
		two_stepper,orbit,mystream);


	gsl_vector_free(yt);
	gsl_vector_free(yp);

	fclose(mystream);

	return 0;
}