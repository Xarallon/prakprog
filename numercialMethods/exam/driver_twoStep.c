#include "odetwo.h"

void driver_twoStep(
	double * t,			// current value of the variable
	double tp,			// Previous value of the variable
	double b,			// end point of the integration
	double h,			// current step size
	gsl_vector * yp,	// first, then used as previous y(t)
	gsl_vector * yt,	// current y(t)
	double acc,			// absolute accuracy goal
	double eps, 		// relative accuracy goal
	void stepper(		// stepper function used
		double t, 			// Current value of the variable
		double tp,			// Previous value of the variable
		double h, 			// the step to be taken
		gsl_vector * yt,	// the current value the sought function
		gsl_vector * yp,	// previous position
		void f(double t, gsl_vector * yt, gsl_vector * dydt), 
						// Right hand side of dydt = f(t,y)
		gsl_vector * yth,	// output: y(t+h)
		gsl_vector * err 	// output: error estimates
	),
	void f(double t, gsl_vector * y, gsl_vector * dydt),
						// right hand side of dydt = f(t,y)
	FILE * file_path
	){

	int n = yt->size, max = 1e3, k = 0;
	int steps = 0;
	double E, y_norm, tau, a = *t;
	gsl_vector * yth = gsl_vector_alloc(n);
	gsl_vector * err = gsl_vector_alloc(n);

	fprintf(stderr, "Something has started\n");
	
	fprintf(file_path,"t\ty1\ty2\n");
	fprintf(file_path,"%g",*t);
	for (int i = 0; i < n; ++i){
		fprintf(file_path, "\t%g",gsl_vector_get(yt,i));
	}
	fprintf(file_path, "\n");


	while (*t < b && steps < 100) {
		fprintf(stderr, "k = %d",k);
		
		if (fabs(*t+h)>fabs(b)) {	// Dont overstep
			h = b - *t;
		}
		if (*t >=b){
			break;
		}

		// twp stepper
		two_stepper(*t,tp,h,yp,yt,f,yth,err);

		//	Check if step is within tolorance tau
		E 	   = gsl_blas_dnrm2(err);
		y_norm = gsl_blas_dnrm2(yth);
		tau = (y_norm * eps + acc) * sqrt(h/(b-a));


		if (E < tau) { // Accecpt step and continue
			k++;
			if (k>max-1){
				fprintf(stderr, "Something went wrong\n");
				break;
			}
			tp = *t;
			*t += h;
			gsl_vector_memcpy(yp,yt);
			gsl_vector_memcpy(yt,yth);
			
			fprintf(file_path,"%g",*t);
			for (int i = 0; i < n; ++i){
				fprintf(file_path, "\t%g",gsl_vector_get(yt,i));
			}
			fprintf(file_path, "\n");		
		}

		// Correct the step
		if (E > 0){
			h *= pow(tau/E, 0.25) * 0.95;
		}
		else {
			h *=2;
		}

		fprintf(stderr, ", t = %.10e",*t);
		fprintf(stderr, ", h = %.10e\n",h);

		fprintf(stderr, "E =  %.10e, tau =  %.10e\n", E, tau);
		steps++;
		
	}


	gsl_vector_free(yth);
	gsl_vector_free(err);
}