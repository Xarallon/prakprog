#include "odetwo.h"

void two_stepper(
	double t, 			// Current value of the variable
	double tp,			// Previous value of the variable
	double h, 			// the step to be taken
	gsl_vector * yt,	// the current value the sought function
	gsl_vector * yp,	// previous position
	void f(double t, gsl_vector * yt, gsl_vector * dydt), 
						// Right hand side of dydt = f(t,y)
	gsl_vector * yth,	// output: y(t+h)
	gsl_vector * err 	// output: error estimates
	){

	int n = yt->size;
	gsl_vector * c 	  = gsl_vector_alloc(n);
	gsl_vector * dydt =	gsl_vector_alloc(n);

	f(t,yt,dydt);

	// Calculating vector c
	double l = t-tp;	// length of the previous step
	for (int i = 0; i < n; ++i){
		gsl_vector_set(c,i,
			(gsl_vector_get(yp,i) - gsl_vector_get(yt,i)
			+ gsl_vector_get(dydt,i) * l )/l/l);
	}

	for (int i = 0; i < n; ++i){
		gsl_vector_set(yth,i,
			gsl_vector_get(yt,i) 
			+ gsl_vector_get(dydt,i)*h 	// t+h-t=h
			+ gsl_vector_get(c,i)	*h*h);
	}

	for (int i = 0; i < n; ++i){
		gsl_vector_set(err,i, gsl_vector_get(c,i)*h*h);
	}

	gsl_vector_free(c);
	gsl_vector_free(dydt);
}