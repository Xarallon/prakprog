#ifndef HAVE_ODETWO

#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

void two_stepper(
	double t, 			// Current value of the variable
	double tp,			// Previous value of the variable
	double h, 			// the step to be taken
	gsl_vector * yt,	// the current value the sought function
	gsl_vector * yp,	// previous position
	void f(double t, gsl_vector * yt, gsl_vector * dydt), 
						// Right hand side of dydt = f(t,y)
	gsl_vector * yth,	// output: y(t+h)
	gsl_vector * err 	// output: error estimates
	);

void driver_twoStep(
	double * t,			// current value of the variable
	double tp,			// Previous value of the variable
	double b,			// end point of the integration
	double h,			// current step size
	gsl_vector * yp,	// first, then used as previous y(t)
	gsl_vector * yt,	// current y(t)
	double acc,			// absolute accuracy goal
	double eps, 		// relative accuracy goal
	void stepper(		// stepper function used
		double t, 			// Current value of the variable
		double tp,			// Previous value of the variable
		double h, 			// the step to be taken
		gsl_vector * yt,	// the current value the sought function
		gsl_vector * yp,	// previous position
		void f(double t, gsl_vector * yt, gsl_vector * dydt), 
						// Right hand side of dydt = f(t,y)
		gsl_vector * yth,	// output: y(t+h)
		gsl_vector * err 	// output: error estimates
	),
	void f(double t, gsl_vector * y, gsl_vector * dydt),
						// right hand side of dydt = f(t,y)
	FILE * file_path
	);

void rkstep12(
	double t, 			// Current value of the variable
	double h, 			// the step to be taken
	gsl_vector * yt,	// the current value the sought function
	void f(double t, gsl_vector * yt, gsl_vector * dydt), 
						// Right hand side of dydt = f(t,y)
	gsl_vector * yth,	// output: y(t+h)
	gsl_vector * err 	// output: error estimates
	);

	void orbit(double t, gsl_vector * y, gsl_vector * dydt);

	void harm_osc(double x, gsl_vector * y, gsl_vector * dydx);

   
#define HAVE_ODETWO
#endif