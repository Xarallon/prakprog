#include "linEq.h"

void randomize_matrix(gsl_matrix* M){
	for (int i = 0; i < M->size1; ++i){
		for (int j = 0; j < M->size2; ++j){
			gsl_matrix_set(M,i,j,rand() % 10 + 1);
		}
	}
}

void print_matrix(char* s, gsl_matrix* M) {
	printf("%s\n", s);
	for (int i = 0; i < M->size1; ++i){
		for (int j = 0; j < M->size2; ++j){
			printf("%8.3f", gsl_matrix_get(M,i,j));
		}
	printf("\n");	
	}
}

void randomize_vector(gsl_vector* v){
	for (int i = 0; i < v->size; ++i){
		gsl_vector_set(v,i,rand() % 10 + 1);
	}
}

void print_vector(char* s,gsl_vector* v) {
	printf("%s\n",s);
	for (int i = 0; i < v->size; ++i){
		printf("%8.3f\n", gsl_vector_get(v,i));
	}	
}

void backsub(gsl_matrix* U, gsl_vector* c){
	for(int i = c->size -1; i>=0 ; i--){
		double s = gsl_vector_get(c,i);
		for (int k = i+1; k < c->size; ++k){
			s -=gsl_matrix_get(U,i,k) * gsl_vector_get(c,k);
		}
		gsl_vector_set(c,i, s/gsl_matrix_get(U,i,i));
	}
}