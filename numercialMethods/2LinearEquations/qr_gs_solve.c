#include "linEq.h"

void qr_gs_solve(
	const gsl_matrix* Q, const gsl_matrix* R, 
	const gsl_vector* b, gsl_vector* x){
	// Solve QRx=b
	gsl_blas_dgemv(CblasTrans,1.0,Q,b,0,x);
	backsub(R,x);
}