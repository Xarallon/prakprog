#include "linEq.h"

void qr_gs_decom(gsl_matrix * Q, gsl_matrix* R){
	int n = Q->size1; int m = Q->size2;

	// Columns, later columns, helper (rows)
	int i = 0; int j = 0; int k = 0;
	double norm_sum;
	// Run over columns
	for (i = 0; i < m; ++i){
		
		// R_ii = sqrt( q_i^T q_i )
		norm_sum = 0;
		for (k = 0; k < n; ++k){
			norm_sum += gsl_matrix_get(Q,k,i) * gsl_matrix_get(Q,k,i);
		}
		gsl_matrix_set(R,i,i,sqrt(norm_sum));


		// q_i = q_i / R_ii
		for (k = 0; k < n; ++k){
			gsl_matrix_set(Q,k,i,
				gsl_matrix_get(Q,k,i)/gsl_matrix_get(R,i,i));
		}

		// Run over the rest of the columns
		for (j = i+1; j < m; ++j){
			// R_ij = q_i^T q_j;
			norm_sum = 0;
			for (k = 0; k < n; ++k){
				norm_sum += gsl_matrix_get(Q,k,i)*gsl_matrix_get(Q,k,j);
			}
			gsl_matrix_set(R,i,j,norm_sum);

			// q_j = q_j - q_i R_ij
			for (k = 0; k < n; ++k){
				gsl_matrix_set(Q,k,j,gsl_matrix_get(Q,k,j)
					-gsl_matrix_get(Q,k,i)*gsl_matrix_get(R,i,j));
			}
		}
	}
}