#include "linEq.h"

int main(){
	// Part A.1
	printf("Part A.1\n");
	int n = 10; int m = 5;
	gsl_matrix * A = gsl_matrix_alloc(n,m);
	gsl_matrix * Q = gsl_matrix_alloc(n,m);
	gsl_matrix * R = gsl_matrix_calloc(m,m);
	
	// Make matrices
	randomize_matrix(A);
	gsl_matrix_memcpy(Q,A);

	print_matrix("Matrix A",A);
	print_matrix("Matrix Q",Q);

	qr_gs_decom(Q,R);

	// Check R Triangular
	print_matrix("Matrix R",R);
	print_matrix("Matrix Q", Q);

	// Check Q^T Q = 1
	gsl_matrix * QTQ = gsl_matrix_alloc(m,m);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0,Q,Q,0,QTQ);
	print_matrix("Matrix Q^T Q",QTQ);
	
	// Check QR = A
	gsl_matrix * A2 = gsl_matrix_alloc(n,m);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,Q,R,0,A2);
	print_matrix("Matrix A",A);
	print_matrix("Matrix A2",A2);


	gsl_matrix_free(A);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_matrix_free(QTQ);
	gsl_matrix_free(A2);


	// Part A.2
	printf("\n\nPart A.2\n");

	// Make matrices and vectors
	gsl_matrix * B = gsl_matrix_alloc(n,n);
	gsl_matrix * R2 = gsl_matrix_calloc(n,n);
	randomize_matrix(B);
	gsl_matrix * Bcpy = gsl_matrix_alloc(n,n);
	gsl_matrix_memcpy(Bcpy,B);
	gsl_vector * b = gsl_vector_alloc(n);
	gsl_vector * x = gsl_vector_alloc(n);
	randomize_vector(b);
	

	print_matrix("Matrix B",B);
	print_vector("Vector b",b);


	qr_gs_decom(B,R2);
	print_matrix("Matrix Q",B);
	print_matrix("Matrix R",R2);

	qr_gs_solve(B,R2,b,x);

	// Check Bx=b
	gsl_vector * Bx = gsl_vector_alloc(n);
	gsl_blas_dgemv(CblasNoTrans,1.0,Bcpy,x,0,Bx);
	print_vector("Vector Bx",Bx);

	// Free
	gsl_matrix_free(B);
	gsl_matrix_free(Bcpy);
	gsl_matrix_free(R2);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_vector_free(Bx);

	return 0;
}