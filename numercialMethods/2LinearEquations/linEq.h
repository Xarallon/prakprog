#ifndef HAVE_LINEQ

#include<stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

void qr_gs_decom(gsl_matrix * Q, gsl_matrix* R);
void print_matrix(char* s, gsl_matrix* M);
void randomize_matrix(gsl_matrix* M);
void randomize_vector(gsl_vector* v);
void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector*, gsl_vector* x);
void print_vector(char* s,gsl_vector* v);
void backsub(gsl_matrix* U, gsl_vector* c);


#define HAVE_LINEQ
#endif