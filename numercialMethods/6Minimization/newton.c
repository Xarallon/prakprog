#include "min.h"


void newton(
	double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H), /* f: objective function, df: gradient, H: Hessian matrix H*/
	gsl_vector* xstart, /* starting point, becomes the latest approximation to the root on exit */
	double eps, int * steps){ /* accuracy goal, on exit |gradient|<eps */


	int n = xstart->size;
	gsl_vector * df = gsl_vector_alloc(n);
	gsl_vector *  s = gsl_vector_alloc(n);
	gsl_vector * xs = gsl_vector_alloc(n); // x+s
	gsl_vector * dummy = gsl_vector_alloc(n);
	gsl_matrix *  H = gsl_matrix_alloc(n,n);
	gsl_matrix *  R = gsl_matrix_alloc(n,n);

	double alpha = pow(10,-4);

	double phi = f(xstart,df,H);
	double phi_xs;
	double res;
	int max_steps = pow(10,6);
	double lambda;
	double lambda_min = 1.0/64;

	int notArmijo;
	int unconverged = 1;

	while (unconverged){

		print_vector("xstart",xstart);
		print_vector("df",df);
		printf("f(xstart) = %g\n",phi);

		// solve nabla phi(x) + H(x) deltaX = 0
		qr_gs_decom(H,R);
		qr_gs_solve(H,R,df,s);	// s take place of deltaX
		gsl_vector_scale(s,-1);

		lambda = 1;

		// Making xs: xs = s, xs = xs + xstart
		gsl_vector_memcpy(xs,s);
		gsl_vector_add(xs,xstart);
		phi_xs = f(xs,dummy,H);

		gsl_blas_ddot(s,df,&res);
		notArmijo = phi_xs >= ( phi + alpha * res);
		while (notArmijo && lambda > lambda_min) {
			
			lambda /=2;
			gsl_vector_scale(s,0.5);

			gsl_blas_ddot(s,df,&res); // res = s^T df

			// Making xs: xs = s, xs = xs + xstart
			gsl_vector_memcpy(xs,s);
			gsl_vector_add(xs,xstart);
			notArmijo = f(xs,dummy,H) >= (phi + alpha * res);
		}

		// Take the step
		gsl_vector_memcpy(xstart,xs);
		*steps +=1 ;
		printf("Step: %i\n", *steps);

		phi = f(xstart,df,H);

		unconverged = gsl_blas_dnrm2(df) >= eps; // norm of gradient > eps
		unconverged = unconverged && (*steps < max_steps);
	}

	gsl_vector_free(df);
	gsl_vector_free(s);
	gsl_vector_free(xs);
	gsl_vector_free(dummy);
	gsl_matrix_free(H);
	gsl_matrix_free(R);
}