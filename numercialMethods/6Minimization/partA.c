#include "min.h"

int main(int argc, char const *argv[]){

	// vector x = alloc

	int n = 2;
	gsl_vector * xstart = gsl_vector_alloc(n);

	gsl_vector_set(xstart,0,3.6);
	gsl_vector_set(xstart,1,-2);

	double eps = pow(10,-6);
	int steps = 0;

	print_vector("vector xmin",xstart);	
	newton(Himmelblau_hessian,xstart,eps,&steps);

	print_vector("vector xmin for Himmelblau_hessian",xstart);

	steps = 0;

	gsl_vector_set(xstart,0,100);
	gsl_vector_set(xstart,1,-10);

	print_vector("vector xstart",xstart);

	newton(Rosenbrock_hessian,xstart,eps, &steps);

	print_vector("vector xmin for Rosenbrock_hessian",xstart);

	
	// free(x)
	gsl_vector_free(xstart);
	
	
	return 0;
}