#include "min.h"

double Himmelblau_hessian(gsl_vector * v, gsl_vector * df, gsl_matrix * H){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);

	gsl_vector_set(df,0, 4*x*(x*x+y-11) + 2*(x+y*y-7) );
	gsl_vector_set(df,1, 2*(x*x+y-11) + 4*y*(x+y*y-7) );

	gsl_matrix_set(H,0,0, 12*x*x + 4*y -42 );
	gsl_matrix_set(H,0,1, 4*(x+y) );
	gsl_matrix_set(H,1,0, 4*(x+y) );
	gsl_matrix_set(H,1,1, 4*x + 12*y*y -26 );

	return (x*x+y-11)*(x*x+y-11) + (x+y*y-7)*(x+y*y-7);
}


double Rosenbrock_hessian(gsl_vector * v, gsl_vector * df, gsl_matrix * H){

	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);

	gsl_vector_set(df,0, 2*(x-1) -400*x*(y-x*x) );
	gsl_vector_set(df,1, 200*(y-x*x));

	//gsl_vector_set(df,0, 2* ((-1) + x + 200 * x*x*x - 200 * x * y));/* dfdx */
	//gsl_vector_set(df,1, 200 *(y-x*x) );

	gsl_matrix_set(H,0,0, 1200*x*x-400*y+2 );
	gsl_matrix_set(H,0,1, -400*x );
	gsl_matrix_set(H,1,0, -400*x );
	gsl_matrix_set(H,1,1, 200 );


	// gsl_matrix_set(H,0,0, 2 + 1200 * x*x - 400 * y );	/* d²f/dx²	*/
	//gsl_matrix_set(H,0,1, -400*x); /* d/dy(df/dx) = d/dx(df/dy) */
	//gsl_matrix_set(H,1,0, -400*x); /* -> Symmetric Matrix 		*/
	//gsl_matrix_set(H,1,1, 200);							/* d²f/dy²	*/

	return (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
	//return ( (1-x)*(1-x) ) + 100 * ( (y-(x*x))*(y-(x*x)) );
}