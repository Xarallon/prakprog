#ifndef HAVE_MIN

#include <math.h>
#include "../2LinearEquations/linEq.h"

void newton(
	double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H), /* f: objective function, df: gradient, H: Hessian matrix H*/
	gsl_vector* xstart, /* starting point, becomes the latest approximation to the root on exit */
	double eps, int * steps); /* accuracy goal, on exit |gradient|<eps */


double Himmelblau_hessian(gsl_vector * v, gsl_vector * df, gsl_matrix * H);

double Rosenbrock_hessian(gsl_vector * v, gsl_vector * df, gsl_matrix * H);


#define HAVE_MIN
#endif