#include<stdio.h>

double linterp(int n, double *x, double *y, double z);
double linterp_integ(int n, double *x, double *y, double z);

int main() {
	double x[5] = {-2, -1, 0, 1, 2};
	double y[5] = {4, 1, 0, 1, 4};

	double result;
	result = linterp(5, x,y,1.5);
	printf("result = %g\n", result);

	result = linterp_integ(5,x,y,-1.5);
	printf("result = %g\n", result);

	result = linterp_integ(5,x,y,-0.5);
	printf("result = %g\n", result);

	result = linterp_integ(5,x,y,0.5);
	printf("result = %g\n", result);

	result = linterp_integ(5,x,y,1.5);
	printf("result = %g\n", result);


	return 0;
}