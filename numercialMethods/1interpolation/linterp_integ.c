#include<assert.h>

double area_lin(double x1, double x2, double y1, double y2);
int binarySearch(int n, double* x, double z);
double linterp(int n, double *x, double *y, double z);

double linterp_integ(int n, double *x, double *y, double z) {
	assert(n>1 && z>=x[0] && z<=x[n-1]);

	double result = 0;

	int m = binarySearch(n, x, z);

	/*
		integrate from x[0] to x[m]
	*/
	int j = 0;
	while(j<m){
		result += area_lin(x[j],x[j+1],y[j],y[j+1]);
		j++;
	}

	double fofz = linterp(n,x,y,z);

	result += area_lin(x[j],z,y[j],fofz);

	return result;
}