#include<assert.h>

int binarySearch(int n, double* x, double z);

double linterp(int n, double *x, double *y, double z){
	assert(n>1 && z>=x[0] && z<=x[n-1]);

	int m = binarySearch(n, x, z);

	return y[m] + (y[m+1]-y[m]) / (x[m+1] - x[m]) * (z - x[m]);
}