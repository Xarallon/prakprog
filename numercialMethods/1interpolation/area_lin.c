
double area_lin(double x1, double x2, double y1, double y2){
	double slope = (y2 - y1) / (x2 - x1);
	double cut = - x1 * slope + y1;

	double F_x1 = slope / 2 * x1 * x1 + cut * x1;
	double F_x2 = slope / 2 * x2 * x2 + cut * x2;

	return F_x2 - F_x1;
}