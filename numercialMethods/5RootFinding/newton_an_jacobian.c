#include "roots.h"

void newton_an_jacobian(
	void f(const gsl_vector * x, gsl_vector * fx, gsl_matrix * J),
	gsl_vector * x, double epsilon, gsl_vector * iteration){

	int n = x->size;
	gsl_vector * fx = gsl_vector_alloc(n);
	gsl_matrix *  J = gsl_matrix_alloc(n,n);
	gsl_matrix *  R = gsl_matrix_alloc(n,n);
	gsl_vector * deltaX = gsl_vector_alloc(n);
	gsl_vector * xdx = gsl_vector_alloc(n);



	double norm_f;
	double norm_fdx;
	double lambda;
	double lambda_min = 1.0/64;
	
	int unconverged = 1; 		// ||f(x)|| < epsilon
	int condition_reached;	//
	
	int step = 0;
	int max_step = 1000;
	int funcall = 1;

	f(x,fx,J);

	while (unconverged) {

		// Solve J delta x = - f(x) for delta x;
		qr_gs_decom(J,R);
		qr_gs_solve(J,R,fx,deltaX);
		gsl_vector_scale(deltaX,-1);

		norm_f = gsl_blas_dnrm2(fx);

		lambda = 1;

		// Make xdx and f(x+dx)
		gsl_vector_memcpy(xdx,deltaX);
		//print_vector("Vector deltaX",deltaX);
		gsl_vector_add(xdx,x);
		funcall++;
		f(xdx,fx,J);
		norm_fdx = gsl_blas_dnrm2(fx);

		condition_reached = (norm_fdx > (1-lambda/2) * norm_f);
		while (condition_reached && (lambda > lambda_min)) {
			
			lambda /=2;
			gsl_vector_scale(deltaX,0.5);

			gsl_vector_memcpy(xdx,deltaX);
			gsl_vector_add(xdx,x);
			funcall++;
			f(xdx,fx,J);
			norm_fdx = gsl_blas_dnrm2(fx);
			//printf("lambda = %.10g\n",lambda);
			//print_vector("Vector xdx",xdx);
			condition_reached = (norm_fdx > (1-lambda/2) * norm_f);
		}
		
		//print_vector("Vector xdx",xdx);
		gsl_vector_memcpy(x,xdx);
		step++;
		//print_vector("Vector x",x);
		f(x,fx,J);

		norm_f = gsl_blas_dnrm2(fx);
		unconverged = (norm_f >= epsilon) && (step < max_step); // norm f(x) < epsilon;
		
		//print_vector("Vector x",x);
		//printf("lambda = %.10g\n\n\n",lambda);

	}

	gsl_vector_set(iteration,0,step);
	gsl_vector_set(iteration,1,funcall);

	//print_vector("Vector fx",fx);

	//print_vector("Vector x",x);
	//print_vector("Vector fx",fx);
	//print_matrix("Matrix J",J);

	gsl_vector_free(fx);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(deltaX);
	gsl_vector_free(xdx);


}
