#include "roots.h"

int main(int argc, char const *argv[]){
	
	int n = 2;
	gsl_vector * x = gsl_vector_alloc(n);
	gsl_vector * i = gsl_vector_alloc(n);


	double epsilon = pow(10,-6);
	gsl_vector_set(x,0,-10);
	gsl_vector_set(x,1,10);

	print_vector("Vector start",x);

	//newton_an_jacobian(Himmelblau_an_jacobian, x, epsilon);
	newton_an_jacobian(Rosenbrock_an_jacobian,x,epsilon,i);

	print_vector("Vector end",x);
	double a = gsl_vector_get(x,0);
	double b = gsl_vector_get(x,1);
	printf("Function value: %g \n", (1-a)*(1-a)+100*(b-a*a)*(b-a*a) );
	print_vector("Steps and Function calls",i);


	gsl_vector_free(x);
	gsl_vector_free(i);

	return 0;
}