#ifndef HAVE_ROOTS

#include "../2LinearEquations/linEq.h"

void newton_an_jacobian(
	void f(const gsl_vector * x, gsl_vector * fx, gsl_matrix * J),
	gsl_vector * z, double epsilon, gsl_vector * iteration);

void Himmelblau_an_jacobian(gsl_vector * z, gsl_vector * fx, gsl_matrix * J);

void Rosenbrock_an_jacobian(gsl_vector * z, gsl_vector * fx, gsl_matrix * J);


#define HAVE_ROOTS
#endif