#include "roots.h"

void Himmelblau_an_jacobian(gsl_vector * z, gsl_vector * fx, gsl_matrix * J){

	double x = gsl_vector_get(z,0);
	double y = gsl_vector_get(z,1);

	gsl_vector_set(fx,0, 4*x*(x*x+y-11)+2*(x+y*y-7) );
	gsl_vector_set(fx,1, 2*(x*x+y-11)+4*y*(x+y*y-7) );

	gsl_matrix_set(J,0,0, 4*(3*x*x+y-11)+2 );
	gsl_matrix_set(J,0,1, 4*x+4*y );
	gsl_matrix_set(J,1,0, 4*x+4*y );
	gsl_matrix_set(J,1,1, 2+4*(x+3*y*y-7) );
}

void Rosenbrock_an_jacobian(gsl_vector * z, gsl_vector * fx, gsl_matrix * J){

	double x = gsl_vector_get(z,0);
	double y = gsl_vector_get(z,1);

	gsl_vector_set(fx,0, 2*(x-1) -400*x*(y-x*x) );
	gsl_vector_set(fx,1, 200*(y-x*x) );

	gsl_matrix_set(J,0,0, 2 - 400*(y-3*x*x) );
	gsl_matrix_set(J,0,1, -400*x );
	gsl_matrix_set(J,1,0, -400*x );
	gsl_matrix_set(J,1,1, 200 );
}