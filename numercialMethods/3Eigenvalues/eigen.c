#include "eigen.h"

void random_sym_matrix(gsl_matrix* M){
	for (int i = 0; i < M->size1; ++i){
		for (int j = 0; j < M->size2; ++j){
			int rnd = rand() % 10 + 1;
			gsl_matrix_set(M,i,j,rnd);
			gsl_matrix_set(M,j,i,rnd);
		}
	}
}

void print_matrix(char* s, gsl_matrix* M) {
	printf("%s\n", s);
	for (int i = 0; i < M->size1; ++i){
		for (int j = 0; j < M->size2; ++j){
			printf("%8.3f", gsl_matrix_get(M,i,j));
		}
	printf("\n");	
	}
}

void print_vector(char* s, gsl_vector* v) {
	printf("%s\n", s);
	for (int i = 0; i < v->size; ++i){
		printf("%8.3f", gsl_vector_get(v,i));
	}
	printf("\n");
}
