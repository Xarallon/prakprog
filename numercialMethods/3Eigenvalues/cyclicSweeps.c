#include "eigen.h"

int main(int argc, char const *argv[]){

	int n = 10;
	gsl_matrix * A = gsl_matrix_alloc(n,n);
	gsl_matrix * Acpy = gsl_matrix_alloc(n,n);
	gsl_matrix * V = gsl_matrix_alloc(n,n);
	gsl_vector * e = gsl_vector_alloc(n);

	random_sym_matrix(A);
	gsl_matrix_memcpy(Acpy,A);
	print_matrix("Matrix A",A);
	print_matrix("Matrix Acpy",Acpy);


	int sweeps;

	sweeps = jacobiSweep(A,e,V);

	printf("Number of sweeps: %i\n", sweeps);

	print_matrix("Sweeped matrix M",A);

	print_matrix("Matrix V",V);

	gsl_matrix * AV = gsl_matrix_calloc(n,n);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0,Acpy,V,0,AV);
	gsl_matrix * VTAV = gsl_matrix_calloc(n,n);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,AV,0,VTAV);

	print_matrix("Matrix VTAV",VTAV);

	print_vector("Vector e",e);





	gsl_matrix_free(A);
	gsl_vector_free(e);
	gsl_matrix_free(V);
	gsl_matrix_free(AV);
	gsl_matrix_free(VTAV);
	gsl_matrix_free(Acpy);

	return 0;
}