#ifndef HAVE_EIGEN

#include<stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

void print_matrix(char* s, gsl_matrix* M);
void random_sym_matrix(gsl_matrix* M);
int jacobiSweep(gsl_matrix * A, gsl_vector * e , gsl_matrix * V);
void print_vector(char* s, gsl_vector* V);

#define HAVE_EIGEN
#endif