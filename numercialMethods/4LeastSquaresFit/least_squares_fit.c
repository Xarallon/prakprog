#include "lsfit.h"

void least_squares_fit(int m, double funs(int i, double xi), 
	gsl_vector * x, gsl_vector * y, gsl_vector * dy, gsl_vector * c){

	int n = x->size;
	gsl_matrix * Q = gsl_matrix_alloc(n,m);
	gsl_matrix * R = gsl_matrix_alloc(m,m);
	gsl_vector * b = gsl_vector_alloc(n);

	for (int i = 0; i < n; ++i){
		double xi = gsl_vector_get(x,i);
		double yi = gsl_vector_get(y,i);
		double dyi = gsl_vector_get(dy,i);

		gsl_vector_set(b,i,yi/dyi);

		for (int j = 0; j < m; ++j){
			gsl_matrix_set(Q,i,j,funs(j,xi)/dyi);
		}
	}

	qr_gs_decom(Q,R);
	qr_gs_solve(Q,R,b,c);

	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_vector_free(b);
}