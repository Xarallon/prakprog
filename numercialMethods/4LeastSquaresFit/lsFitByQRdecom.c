#include "lsfit.h"

int main(int argc, char const *argv[]){

	// Define data
	int n = 9; int m = 3;
	double x_data[] = { 0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9};
	double y_data[] = { -15.3, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75};
	double dy_data[] = { 1.04, 0.594, 0.983, 0.998, 1.11, 0.368, 0.535, 0.968, 0.478};

	// Ready data
	gsl_vector * x  = gsl_vector_alloc(n);
	gsl_vector * y  = gsl_vector_alloc(n);
	gsl_vector * dy = gsl_vector_alloc(n);
	for (int i = 0; i < n; ++i){
		gsl_vector_set(x,i,x_data[i]);
		gsl_vector_set(y,i,y_data[i]);
		gsl_vector_set(dy,i,dy_data[i]);
	}

	// Setup funs
	double funs(int i, double xi) {
		switch(i){
			case 0: return log(xi); break;
			case 1: return 1.0; break;
			case 2: return xi; break;
			default: fprintf(stderr, "Wrong i: %i\n", i);
		}
	}

	gsl_vector * c = gsl_vector_alloc(m);

	least_squares_fit(m, funs, x, y, dy, c);
	print_vector("Vector c",c);

	gsl_vector_free(x);
	gsl_vector_free(y);
	gsl_vector_free(dy);
	gsl_vector_free(c);


	return 0;
}