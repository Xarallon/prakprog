#ifndef HAVE_LSFIT

#include "../2LinearEquations/linEq.h"

void least_squares_fit(int m, double funs(int i, double xi), 
	gsl_vector * x, gsl_vector * y, gsl_vector * dy, gsl_vector * c);

#define HAVE_LSFIT
#endif