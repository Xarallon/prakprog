#ifndef HAVE_ADAPT

#include <stdio.h>
#include <math.h>


double adapt24(
	double f(double),		// function to be integrated
	double a, double b,		// start and end point
	double acc, double eps,	// absolute and relative accuracy
	double f2, double f3,	// recursion
	double * err,			// error
	int nrec);


double adapt(
	double f(double),
	double a, double b,
	double acc, double eps,
	double * err);			// error
   
#define HAVE_ADAPT
#endif