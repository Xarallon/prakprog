#include "adapt.h"

int main(int argc, char const *argv[]){
	
	int calls = 0;
	double a = 0, b = 1, acc = 1e-4, eps = 1e-4;
	double err = 0;

	double f(double x) {calls++; return 1/sqrt(x);};

	double Q = adapt(f,a,b,acc,eps, &err );

	printf("Q = %g, err = %g, calls = %d\n", Q, err, calls);

	calls = 0;
	double g(double x) {calls++; return sqrt(x);};
	Q = adapt(g,a,b,acc,eps,&err);
	printf("Q = %g, err = %g, calls = %d\n", Q, err, calls);

	calls = 0;
	double h(double x) {calls++; return log(x)/sqrt(x);};
	Q = adapt(h,a,b,acc,eps,&err);
	printf("Q = %g, err = %g, calls = %d\n", Q, err, calls);

	calls = 0;
	acc = 1e-10; eps = 1e-10;
	double fun(double x) {calls++; return 4*sqrt(1-(1-x)*(1-x));};
	Q = adapt(fun,a,b,acc,eps,&err);
	printf("Q = %.15g, err = %g, calls = %d\n", Q, err, calls);
	printf("Q - pi = %.10g\n", Q-M_PI);

	return 0;
}