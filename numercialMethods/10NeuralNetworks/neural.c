#include "neural.h"


ann* ann_alloc(int number_of_hidden_neurons, 
	double(*activation_function)(double)){

	ann* network = malloc(sizeof(ann));

	network->n=number_of_hidden_neurons;
	network->f=activation_function;
	network->a = gsl_vector_alloc(number_of_hidden_neurons);
	network->b = gsl_vector_alloc(number_of_hidden_neurons);
	network->w = gsl_vector_alloc(number_of_hidden_neurons);

	return network;
}

void ann_free(ann* network){
	gsl_vector_free(network->a);
	gsl_vector_free(network->b);
	gsl_vector_free(network->w);
	free(network);
}


double ann_feed_forward(ann* network, double x){
	double res = 0;
	
	for (int i = 0; i < network->n; i++){
		double ai = gsl_vector_get(network->a,i);
		double bi = gsl_vector_get(network->b,i);
		double wi = gsl_vector_get(network->w,i);
		res += network->f((ai * x ) / bi) * wi;
	}

	return res;
}


void ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist){
	const gsl_multimin_fminimizer_nmsimplex2rand * T;

	gsl_multimin_function_fdf my_func;

	my_func.n = 1;



}