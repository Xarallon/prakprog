#include "neural.h"

double activation_function(double x) {return exp(-x*x);}

int main(int argc, char const *argv[]){
	
	int n = 3;

	ann * network = ann_alloc(n, activation_function);

	for (int i = 0; i < network->n; ++i){
		gsl_vector_set(network->a,i,1);
		gsl_vector_set(network->b,i,1);
		gsl_vector_set(network->w,i,3);
	}

	double x = 1;
	
	double y = ann_feed_forward(network,x);
	printf("x=1, y=%g\n", y);
	x = 2;
	y = ann_feed_forward(network,x);
	printf("x=2, y=%g\n", y);



	ann_free(network);

	return 0;
}