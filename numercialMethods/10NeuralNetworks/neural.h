#ifndef HAVE_NEURAL

#include<stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>
#include <gsl_multimin.h>

typedef struct {int n; double (*f)(double); 
	gsl_vector* a; gsl_vector* b; gsl_vector* w;} ann;
ann* ann_alloc(int number_of_hidden_neurons, double(*activation_function)(double));
void ann_free(ann* network);
double ann_feed_forward(ann* network, double x);
void ann_train(ann* network, gsl_vector* xlist, gsl_vector* ylist);

#define HAVE_NEURAL
#endif