#include "mcinteg.h"

int main(int argc, char const *argv[]){
	
	double err, res;
	int dim = 2;

	gsl_vector * a = gsl_vector_alloc(dim);
	gsl_vector * b = gsl_vector_alloc(dim);
	gsl_vector_set(a,0, -1);
	gsl_vector_set(a,1, -1);
	gsl_vector_set(b,0, 1);
	gsl_vector_set(b,1, 1);

	int n = 1e7;

	plainMC(bullet,a,b,n,&res,&err);
	printf("Monte Carlo integration of f(x,y)=-x^2-y^2+5/3\n");
	printf("Result on the square [-1,1]x[-1,1] = %g , and error = %g\n",res,err);

	plainMC(saddle,a,b,n,&res,&err);
	printf("Monte Carlo integration of f(x,y)=x^2-y^2+1\n");
	printf("Result on the square [-1,1]x[-1,1] = %g , and error = %g\n",res,err);


	dim = 3;
	gsl_vector * a2 = gsl_vector_alloc(dim);
	gsl_vector * b2 = gsl_vector_alloc(dim);
	gsl_vector_set(a2,0, 0);
	gsl_vector_set(a2,1, 0);
	gsl_vector_set(a2,2, 0);
	gsl_vector_set(b2,0, M_PI);
	gsl_vector_set(b2,1, M_PI);
	gsl_vector_set(b2,2, M_PI);

	double f(gsl_vector * v) {
		return 1/(1.0
			-cos(gsl_vector_get(v,0))
			*cos(gsl_vector_get(v,1))
			*cos(gsl_vector_get(v,2))
			)/M_PI/M_PI/M_PI;
	}

	plainMC(f,a2,b2,1e8,&res,&err);
	printf("%.12g\n", res);

	double res2 = 1.3932039296856768591842462603255;
	printf("%.12g\n", res2);



	gsl_vector_free(a);
	gsl_vector_free(b);
	gsl_vector_free(a2);
	gsl_vector_free(b2);

	return 0;
}