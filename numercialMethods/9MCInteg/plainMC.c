#include "mcinteg.h"

#define RND ((double) rand()/RAND_MAX)

void plainMC(
	double f(gsl_vector * v),				// Integrand
	gsl_vector * a, gsl_vector * b,			// End points
	int n, double * result, double * err){	// #points, result and error
	
	int dim = a->size;
	double V = 1;
	for (int i = 0; i < dim; ++i){
		V *= gsl_vector_get(b,i) - gsl_vector_get(a,i);
	}

	double sum = 0, sum2 = 0, fx;

	gsl_vector * x = gsl_vector_alloc(dim);

	for (int i = 0; i < n; ++i){
		// Randomize x value
		for (int i = 0; i < dim; ++i){
			gsl_vector_set(x,i, gsl_vector_get(a,i)
			 + RND *( gsl_vector_get(b,i) - gsl_vector_get(a,i) ));
		}

		fx = f(x);
		sum += fx;
		sum2 += fx*fx;
	}

	gsl_vector_free(x);

	double avr = sum/n, var = sum2 / n - avr*avr;
	*result = avr*V;
	*err = sqrt(var / n)*V;
}

