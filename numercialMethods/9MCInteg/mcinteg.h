#ifndef HAVE_MC

#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <math.h>

void plainMC(
	double f(gsl_vector * v),				// Integrand
	gsl_vector * a, gsl_vector * b,			// End points
	int n, double * result, double * err);	// #points, result and error

double bullet(gsl_vector * v);

double saddle(gsl_vector * v);
   
#define HAVE_MC
#endif