#include "mcinteg.h"

double bullet(gsl_vector * v){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	return -x*x -y*y + 5.0/3;
}


double saddle(gsl_vector * v){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	return x*x -y*y + 1;
}