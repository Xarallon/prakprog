#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>

void matrix_print(const char* s,const gsl_matrix* m){	// Prints matrix
	printf("\n%s\n",s);
	for(int j=0;j<m->size2;j++){	// '->' dereferences m - size2 = column
		for(int i=0;i<m->size1;i++)	//	size1 = row
			printf("%8.3g ",gsl_matrix_get(m,i,j));
		printf("\n");
	}
}
void vector_print(const char* s, const gsl_vector* v){
	printf("\n%s\n",s);
	gsl_vector_fprintf(stdout, v, "%g");

}


int main(int argc, char const *argv[]){

	gsl_matrix * A = gsl_matrix_alloc(3,3);
	gsl_vector * b = gsl_vector_alloc(3);
	gsl_vector * x = gsl_vector_alloc(3);

	gsl_vector * R = gsl_vector_alloc(3);
	gsl_matrix * Q = gsl_matrix_alloc(3,3);
	gsl_vector * y = gsl_vector_alloc(3);

	gsl_matrix_set(A,0,0,  6.13); 
	gsl_matrix_set(A,1,0,  8.08);
	gsl_matrix_set(A,2,0, -4.34);

	gsl_matrix_set(A,0,1, -2.90); 
	gsl_matrix_set(A,1,1, -6.31); 
	gsl_matrix_set(A,2,1,  1.00); 

	gsl_matrix_set(A,0,2,  5.86);
	gsl_matrix_set(A,1,2, -3.89);
	gsl_matrix_set(A,2,2,  0.19);

	gsl_vector_set(b,0, 6.23);
	gsl_vector_set(b,1, 5.37);
	gsl_vector_set(b,2, 2.29);

	gsl_matrix_memcpy(Q,A);

	gsl_linalg_QR_decomp(Q,R);
	gsl_linalg_QR_solve(Q,R,b,x);

	printf("System Ax = b\n");
	matrix_print("Matrix A",A);
	vector_print("Vector b",b);

	printf("Solving with QR decmposition\n");
	matrix_print("Matrix Q", Q);
	vector_print("\"Matrix\" R",R);

	vector_print("Vector x",x);

	gsl_blas_dgemv(CblasNoTrans, 1.0, A, x, 0, y);
	vector_print("Vector y = Ax",y);

	vector_print("Vector b",b);

	int vectors_equal = gsl_vector_equal(y,b);
	if (vectors_equal) {
		printf("Test passed \n");
	}
	else {
		printf("Test not passed\n");
		printf("The precision could be the issue\n");
	}
	
	gsl_matrix_free(A);
	gsl_matrix_free(Q);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_vector_free(y);
	gsl_vector_free(R);

	return 0;
}