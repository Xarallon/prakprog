#include <stdio.h>
#include <gsl/gsl_sf_airy.h>

int main(int argc, char const *argv[]){

	double Ai, Bi;
	
	for (double x = -15; x < 5; x+=0.01){
		Ai = gsl_sf_airy_Ai(x,GSL_PREC_APPROX);
		Bi = gsl_sf_airy_Bi(x,GSL_PREC_APPROX);

		printf("%g\t%g\t%g\n",x,Ai,Bi);
	}

	return 0;
}