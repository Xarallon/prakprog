#include <stdio.h>
#include <math.h>
#include <gsl/gsl_errno.h> // For GSL_SUCCESS 
#include <gsl/gsl_odeiv2.h>
#include <getopt.h>
#define steptype gsl_odeiv2_step_rk8pd // #define steptype gsl_odeiv2_step_rkf45


int orbit( double angle, const double radial[], double f[], void * params){
	
	(void)(angle);	// Avoid unused parameter warning
	double correction = *(double*) params;
	f[0] = radial[1];
	f[1] = 1 - radial[0] + correction * radial[0]*radial[0];

	return GSL_SUCCESS;
}

int main(int argc, char *argv[]){

	double correction, uprime, angle;

	while(1){
	int opt = getopt(argc, argv, "c:p:");
	if(opt == -1) break;
	switch(opt){
		case 'c': 
			correction=atof(optarg); 
			break;
		case 'p': 
			uprime=atof(optarg);
			break;
		default: /* ? */
                   fprintf(stderr, "Usage: %s [-c correction] [-p uprime]\n", argv[0]);
                   exit(EXIT_FAILURE);

	}
}

	gsl_odeiv2_system sys;
	sys.function = orbit;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void *) &correction;

	
	double hstart = 1e-3,  accuracy = 1e-6, eps = 1e-6;

	gsl_odeiv2_driver * driver = gsl_odeiv2_driver_alloc_y_new (
							&sys, steptype, hstart, accuracy, eps);

	double radial[2] = {1, uprime};
	double angle_max = 39.5 * M_PI, angle_step = 0.05, angle_initial = 0;

	printf("angle\tradial\n");

	for (angle = angle_initial; angle < angle_max; angle += angle_step){
		int status = gsl_odeiv2_driver_apply (driver, &angle_initial, angle, radial);
		printf("%g\t%g\n", angle, radial[0] );
		if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);

	}
	printf("\n\n");


	gsl_odeiv2_driver_free (driver);	// Free allocated driver

}