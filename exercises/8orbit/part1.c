#include <stdio.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>

int func(double x, const double y[], double dydt[],void *params){
	dydt[0] = y[0] * (1-y[0]);
	return GSL_SUCCESS;
}

int main(){
	gsl_odeiv2_system diff_sys;
	diff_sys.function = func;
	diff_sys.jacobian = NULL;
	diff_sys.dimension = 1;
	diff_sys.params = NULL;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	double x_max = 3, delta_x = 0.1;

	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new
			(&diff_sys, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

			
	double xi = 0, y[1], logistic;// 
	y[0] =  0.5;
	for (double x = xi; x < x_max; x += delta_x) {
		
		int status = gsl_odeiv2_driver_apply (driver, &xi, x, y);
		
		logistic = 1.0/(1+exp(-x));

		printf ("%g\t%g\t%g\n", x, y[0], logistic);
		
		if (status != GSL_SUCCESS) {
			fprintf (stderr, "fun: status=%i", status);
		}
	}

	gsl_odeiv2_driver_free (driver);
	return 0;
}