#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>

double integrand_norm(double x, void *params){
	double alpha = *(double*)params;
	double norm_func = exp(-alpha* (x*x));
	return norm_func;
}

double integrand_hamiltonian(double x, void *params){
	double alpha = *(double*)params;
	double hamiltonian_func = 
		( ((-(alpha*alpha) * (x*x))/2) + (alpha/2) + ( (x*x)/2 ) ) 
		* exp(-alpha * (x*x));
	return hamiltonian_func;
}


double expectation_value(double alpha){

	gsl_function norm;
	norm.function = &integrand_norm;
	norm.params =  (void*) &alpha;

	gsl_function hamiltonian;
	hamiltonian.function = &integrand_hamiltonian;
	hamiltonian.params = (void*) &alpha;

	int limit = 1000;
	gsl_integration_workspace *workspace = 
		gsl_integration_workspace_alloc(limit);

	double epsabs = 1e-6, epsrel = 1e-6;
	double result_norm, result_hamiltonian;
	double abserr_norm, abserr_hamiltonian;

	// Infinite integrals qagi
	int status_norm = gsl_integration_qagi(&norm, epsabs, epsrel
		, limit, workspace
		, &result_norm, &abserr_norm );
	int status_hamiltonian = gsl_integration_qagi(&hamiltonian, epsabs, epsrel
		, limit, workspace
		, &result_hamiltonian, &abserr_hamiltonian );

	gsl_integration_workspace_free(workspace);


	double E;

	if(status_norm!=GSL_SUCCESS || status_hamiltonian !=GSL_SUCCESS) 
		E = NAN;
	else
		E = result_hamiltonian / result_norm;
	
	return E;
}

int main(){

	double alpha;
	double a=0.1, b=2*M_PI, dx=0.01;

	printf("alpha\tE\n");

	for(alpha = a; alpha < b; alpha +=dx){
		printf("%g\t%g\n",alpha,expectation_value(alpha));
	}

	return 0;
}