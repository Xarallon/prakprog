#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double integrand(double x, void* params){
	double func = log(x)/sqrt(x);
	return func;
}

int main(){

	gsl_function function;
	function.function = &integrand;
	function.params = NULL;

	int limit = 1000;
	gsl_integration_workspace *workspace = 
		gsl_integration_workspace_alloc(limit);


	double epsabs = 1e-6, epsrel = 1e-6, result, abserr;

	// Use integral with upper bound
	double bound[2] = {0, 1};
	gsl_integration_qags (&function, bound[0], bound[1], epsabs, epsrel
		,limit, workspace, &result, &abserr );
	
		printf("Numerical integral of ln(x)/sqrt(x) = %g\n", result );

	gsl_integration_workspace_free(workspace);

	return 0;
}