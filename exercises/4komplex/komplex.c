#include<stdio.h>
#include<math.h>
#include"komplex.h"

void komplex_print (char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

komplex komplex_new (double x, double y) {
	komplex z = { x, y };
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re , a.im + b.im };
	return result;
}

komplex komplex_sub (komplex a, komplex b) {
	komplex result = { a.re - b.re , a.im - b.im };
	return result;
}

int komplex_equal (komplex a, komplex b, double acc, double eps) {
	// Within acc(uracy)
	if (a.re - b.re > acc) { return 0;};
	if (a.im - b.im > acc) { return 0;};

	// Within eps(ilon) precision
	if ( fabs(a.re-b.re)/(fabs(a.re)+fabs(b.re)) > eps) { return 0;};
	if ( fabs(a.re-b.re)/(fabs(a.re)+fabs(b.re)) > eps) { return 0;};

	// Has to be true
	return 1;
}

komplex komplex_mul (komplex a, komplex b) {
	komplex result = {a.re * b.re - a.im * b.im, 
		a.re * b.im + a.im * b.re};
	return result;
}

komplex komplex_div (komplex a, komplex b) {
	double divisor = b.re * b.re + b.im * b.im;
	komplex result = komplex_mul(a, komplex_conjugate(b));
	result = komplex_new (result.re / divisor, result.im / divisor);
	return result;
}

komplex komplex_conjugate (komplex z) {
	komplex result = {z.re, -z.im};
	return result;
}

double komplex_abs (komplex z) {
	double result = sqrt(z.re * z.re + z.im * z.im);
	return result;
}

/* ... */