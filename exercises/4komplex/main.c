#include"komplex.h"
#include"stdio.h"
#include<math.h>
#define TINY 1e-6

int main(){
	komplex a = {1,2}, b = {3,4};

	printf("testing komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);

	r = komplex_mul(a,b);
	komplex_set(&R,-5, 10);
	komplex_print("a*b should   = ", R);
	komplex_print("a*b actually = ", r);
	
	r = komplex_conjugate(a);
	komplex_set(&R, 1,-2);
	komplex_print("a^* should   = ", r);
	komplex_print("a^* actually = ", R);

	r = komplex_div(b,a);
	komplex_set(&R,2.2, -0.4);
	komplex_print("b/a should   = ", R);
	komplex_print("b/a actually = ", r);

	double res = komplex_abs(a);
	printf("|a| should   = %g\n", sqrt(5));
	printf("|a| actually = %g\n", res);

	int c = komplex_equal(a,a,TINY,TINY);
	printf("a == a should   = %i\n", 1);
	printf("a == a actually = %i\n", c);

	c = komplex_equal(a,b,TINY,TINY);
	printf("a == b should   = %i\n", 0);
	printf("a == b actually = %i\n", c);	



	return 0;
}