#include "stdio.h"
#include "stdlib.h"
#include <gsl/gsl_multimin.h>
#include <gsl/gsl_vector.h>

#define f(t) A*exp(-(t)/T)+B

struct experimental_data {int n; double *t,*y,*e;};

double master_func (const gsl_vector *x, void *params) {
	double  A = gsl_vector_get(x,0);
	double  T = gsl_vector_get(x,1);
	double  B = gsl_vector_get(x,2);

	struct experimental_data *p = (struct experimental_data*) params;
	int     n = p->n;	//	dimension
	double *t = p->t;	//	time
	double *y = p->y;	//	activity
	double *e = p->e;	//	error

	double sum=0;
	for(int i=0;i<n;i++) {
		sum+= pow( (f(t[i]) - y[i] )/e[i] ,2);
	}
	return sum;
}

int main(){
	fprintf(stdout, "\nLeast-squares\n");

	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int dim = sizeof(t)/sizeof(t[0]);

	fprintf(stderr,"time\tactivity\terror\n");
	for(int i=0;i<dim;i++){
		fprintf(stderr,"%g\t%g\t%g\n",t[i],y[i],e[i]);
	}

	fprintf(stderr, "\n\n");

	struct experimental_data params;
	params.n = dim;	//	dimension
	params.t = t; 	//	time
	params.y = y; 	//	activity
	params.e = e;	//	error

	gsl_multimin_function func;
	func.f = master_func;
	func.n = dim;
	func.params = (void*)&params;

	gsl_vector *x = gsl_vector_alloc(func.n);
	gsl_vector_set (x,0,3);
	gsl_vector_set (x,1,2);
	gsl_vector_set (x,2,0);

	gsl_vector *stepsize = gsl_vector_alloc(func.n);
	gsl_vector_set(stepsize,0,2);
	gsl_vector_set(stepsize,1,2);
	gsl_vector_set(stepsize,2,2);

	const gsl_multimin_fminimizer_type *type =
		gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer *s =
	 	gsl_multimin_fminimizer_alloc(type,func.n);
	gsl_multimin_fminimizer_set(s, &func, x, stepsize);

	int iter = 0, status;
	double acc = 1e-1;
	do{
		iter++;
		status = gsl_multimin_fminimizer_iterate(s);

		if (status){
			break;
		}

		status = gsl_multimin_test_size (s->size, acc);

		if (status == GSL_SUCCESS){
			fprintf (stdout,"converged to minimum at\n");
			
			fprintf (stdout, "iter = %i\tA = %.3e\tT = %.3e\t"
				"B = %.3e\tf() = %.3e\tsize = %.3e\n\n",
				iter,
				gsl_vector_get (s->x, 0),
				gsl_vector_get (s->x, 1),
				gsl_vector_get (s->x, 2),
				s->fval, s->size);
		}

	}
	while (status == GSL_CONTINUE && iter < 100);


	double A = gsl_vector_get(s->x,0);
	double T = gsl_vector_get(s->x,1);
	double B = gsl_vector_get(s->x,2);

	fprintf(stderr,"t\tf\n");
	double dt=(t[func.n-1]-t[0])/50;

	for(double ti=t[0]; ti<t[func.n-1]+dt; ti+=dt){
		fprintf(stderr,"%g\t%g\n",ti,f(ti));
	}

		gsl_vector_free (x);
	gsl_vector_free (stepsize);
	gsl_multimin_fminimizer_free (s);

	return EXIT_SUCCESS;
}