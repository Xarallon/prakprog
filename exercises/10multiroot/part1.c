#include "stdio.h"
#include "stdlib.h"
#include <gsl/gsl_multiroots.h>
#include <gsl/gsl_vector.h>

int rosenbrock(const gsl_vector * xy, void *params, gsl_vector * f){

	double x = gsl_vector_get(xy, 0);
	double y = gsl_vector_get(xy, 1);

	double dfdx = 2*(1-x)*(-1)+2*100*(y-(x*x))*(-2*x);
	double dfdy = 2*100*(y-(x*x));

	gsl_vector_set (f,0,dfdx);
	gsl_vector_set (f,1,dfdy);

	return GSL_SUCCESS;
}

int main (void){
	printf("\nFinding the minimum of the Rosenbrock function.\n");

	int dim = 2;

	// Define function
	gsl_multiroot_function f;
	f.f = rosenbrock;
	f.n = dim;
	f.params = NULL;

/*	The solver s is initialized to use this function, 
	with the gsl_multiroot_fsolver_hybrids method*/
	const gsl_multiroot_fsolver_type *T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc (T, dim);

/*	Define variables and their initial guess*/
	double x_init[2] = {2, -2.0};
	gsl_vector *x = gsl_vector_alloc (dim);

	gsl_vector_set (x, 0, x_init[0]);
	gsl_vector_set (x, 1, x_init[1]);

/* 	Set, or reset, an existing solver s 
	to use the function f or function and derivative fdf*/
	gsl_multiroot_fsolver_set (s, &f, x);

	int status, iter = 0;
	double acc = 1e-7;
	do{
	  iter++;
	  status = gsl_multiroot_fsolver_iterate (s);

	  if (status)   /* check if solver is stuck */
	    break;

	  status = gsl_multiroot_test_residual(s->f, acc);
	}
	while (status == GSL_CONTINUE && iter < 1000);
	printf ("status = %s\n", gsl_strerror (status));

	double result_x = gsl_vector_get(s->x, 0);
	double result_y = gsl_vector_get(s->x, 1);

	printf("\nMinimum of Rosenbrock function at\n");
	printf("x\ty\n%g\t%g\n\n", result_x, result_y);
	
/*	Free allocated memory */
	gsl_multiroot_fsolver_free (s);
	gsl_vector_free (x);
	return 0;
}