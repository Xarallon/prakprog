#include <stdio.h>
#include <stdlib.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>
#include <assert.h>


#define steptype gsl_odeiv2_step_rkf45

/* Hydrogen s-wave function */
int hydrogen_s_wave (double r, const double y[], double f[], void* params){
	double energy = *(double*)params;
/*	-(1/2)f'' -(1/r)f = εf */
/*	2nd order eq. as a system of ODE's f = y0 and f' = y1 */
	f[0] = y[1];						/*	f' = y1 */
	f[1] = -2 * (energy+(1/r))*y[0];	/*	f'' = -2*(ε + (1/r))*f */

	return GSL_SUCCESS;
}

double radial(double energy, double r){

	assert (r>=0); /* Function only continues if r >= 0 */

/*	Define function */
	gsl_odeiv2_system H_s_wave;
	H_s_wave.function = hydrogen_s_wave;
	H_s_wave.jacobian = NULL;
	H_s_wave.dimension = 2;
	H_s_wave.params = (void*)&energy;

 /*	Return a pointer to a newly allocated instance of a driver object.
 	Allocates and initialises the evolve,control and stepper objects
 	for the ODE system using a stepper type. 
 	The initial step size is given in hstart*/
	double hstart = 0.1, epsabs = 1e-6, epsrel = 1e-6;
	gsl_odeiv2_driver * driver = 
		gsl_odeiv2_driver_alloc_y_new(&H_s_wave, steptype 
			,hstart, epsabs, epsrel );

/*	Initial conditions. What's up with the 2nd y val? */
	double ri = 1e-3; 		// Initial x
	double y[2] = {ri-ri*ri, 1-2*ri}; 	// Initial y condition
/*					f 			f'	*/

/* 	Apply Driver and solve for f */
	int status = gsl_odeiv2_driver_apply (driver, &ri, r, y);
	if (status != GSL_SUCCESS) fprintf (stdout, "fun: status=%i", status);

/*	Free allocated memory*/
	gsl_odeiv2_driver_free (driver);

//	printf("y[0] = %g\n", y[0] );

	return y[0];
}