#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_multiroots.h>
#include <gsl/gsl_errno.h>

double radial (double energy, double r);

/*	Root function for finding lowest root of radial function	*/
int root_function(const gsl_vector * variable, void *params, gsl_vector *f ){
	double energy = gsl_vector_get(variable,0);
	//assert(energy<0);	// Find negative energy
	double rmax = *(double*)params;
	double fval = radial(energy,rmax);
	gsl_vector_set(f,0,fval);

	return GSL_SUCCESS;
}

int main(int argc, char** argv){	
	double rmax= 8;
	int dim = 1;

/*	Define function 	*/
	gsl_multiroot_function func;
	func.f = root_function;
	func.n = dim;
	func.params = (void*) &rmax;

	const gsl_multiroot_fsolver_type *T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc (T, dim);

/*	Define variables and their initial guess*/
	double x_init = -1;
	gsl_vector *x = gsl_vector_alloc (dim);
	gsl_vector_set(x,0,x_init);

	gsl_multiroot_fsolver_set (s, &func, x);

	int status, iter = 0;
	double acc = 1e-2;

/*	To be printed to hydrogen.tex 	*/
	fprintf(stdout, "Rmax = %g\n", rmax );
	fprintf(stdout,"iter\tenergy\t\tf(rmax)\n");
	do{
		iter++;
		status = gsl_multiroot_fsolver_iterate (s);

	 	if (status)   /* check if solver is stuck */
	    	break;

	  	status = gsl_multiroot_test_residual (s->f, acc);

	  	if(status==GSL_SUCCESS)fprintf(stdout,"converged\n");

		fprintf(stdout,"%i\t%.8g\t%.8g\n",iter,gsl_vector_get(s->x,0),
			gsl_vector_get(s->f,0) );
	}	
	while (status == GSL_CONTINUE && iter < 1000);


	fprintf (stdout,"status = %s\n", gsl_strerror (status));

	double energy =gsl_vector_get(s->x,0);

	fprintf(stdout, "rmax\tenergy\n");
	fprintf(stdout, "%g\t%g\n",rmax, energy);
	fprintf(stdout, "\n\n");

	double exact;
	fprintf(stderr,"r\tf\t\texact\n");
	for (double r = 1e-3; r<rmax; r+=0.02){
		exact = r*exp(-r);
		fprintf(stderr,"%.4g\t%.8g\t%.8g\n", r, radial(energy,r), exact);
	}

	return EXIT_SUCCESS;
}
