#include<tgmath.h>
#include<stdio.h>

int myequal(double a, double b, double tau, double epsilon){


	int absprcsn = fabs(a-b)<tau;
	if (absprcsn){
		printf("True\n");
		return 1;
	}

	int relprcsn = fabs(a-b)/(fabs(a)+fabs(b)) < epsilon / 2;
	if (relprcsn){
		printf("True\n");
		return 1;
	}

	printf("False\n");
	return 0;
}