#include<stdio.h>
#include<limits.h>

int main(){

	printf("\nPart2\n");

	int max = INT_MAX/3;
	printf("N = %i\n", max);

	float sum_up_float = 0, sum_down_float = 0;

	for (int i = 1; i < max+1; ++i)
	{
		sum_up_float += 1.0f/i;
		sum_down_float += 1.0f/(max-i+1);
	}

	printf("Sum up float =   %g\n", sum_up_float);
	printf("Sum down float = %g\n", sum_down_float);

	printf("Rounding errors accumulates.\n");

	double sum_up_double = 0, sum_down_double = 0;

	for (int i = 1; i < max+1; ++i)
	{
		sum_up_double += 1.0/i;
		sum_down_double += 1.0/(max-i+1);
	}

	printf("Sum up double =   %g\n", sum_up_double);
	printf("Sum down double = %g\n", sum_down_double);

	printf("Using double increases precision, \n");

	return 0;
}
