#include<stdio.h>

int myequal(double a, double b, double tau, double epsilon);


int main(){
	printf("\nPart 3\n");
	
	//	Variables
	double a = 50, b = 50.01;
	double tau1 = 0.1, tau2 = 0.005;
	double eps1 = 0.0001, eps2 = 0.001;
	
	
	//	Tests
	printf("Comparing numbers, a = %g and b = %g\n", a, b);
	printf("With low precision, %g\n", tau1);
	int res;
	res = myequal(a, b, tau1, eps1);
	printf("result is %i\n", res);
	printf("With high precision %g\n",tau2);
	res = myequal(a, b, tau2, eps1);
	printf("result is %i\n", res);
	printf("Lowering relative precision from %g to %g\n", eps1, eps2);
	res = myequal(a,b,tau2,eps2);
	printf("Result is %d\n", res);

	return 0;
}