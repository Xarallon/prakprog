#include<limits.h>
#include<float.h>
#include<stdio.h>

int main() {
	printf("\nPart 1 i) \n");
	printf("C-defined max int:            %i\n", INT_MAX);


	int i = 1;
	while(i+1>i) {
		i++;
	}
	printf("My max int, using while:      %i\n", i);


	int j = 1;
	for (int p = 1; p+1>p; ++p)
	{
		j++;
	}
	printf("My max int, using for:        %i\n", j);


	int k = 1;
	do {
		k++;
	} while(k+1>k);
	printf("My max int, using do-while:   %i\n", k);


	printf("Part 1 ii)\n");
	printf("C-defined min int:           %i\n", INT_MIN);
	i = 1;
	while(i-1<i) {
		i--;
	}
	printf("My min int, using while:     %i\n", i);


	j = 1;
	for (int p = 1; p-1<p; --p)
	{
		j--;
	}
	printf("My min int, using for:       %i\n", j);


	k = 1;
	do {
		k--;
	} while(k-1<k);
	printf("My min int, using do-while:  %i\n", k);


	printf("Part 1 iii)\n");
	printf("C-defined FLT_EPSILON:                 %g\n", FLT_EPSILON);
	float xf = 1;
	while(1+xf != 1) {
		xf = xf/2;
	}
	printf("Calculated FLT_EPSILON using while:    %g\n", xf*2);
	xf = 1;
	for(xf=1; 1+xf != 1; xf/=2){}
	printf("Calculated FLT_EPSILON using for:      %g\n", xf*2);
	xf = 1;
	do {
		xf = xf/2;
	} while( 1+xf != 1);
	printf("Calculated FLT_EPSILON using do-while: %g\n", xf*2);



	printf("C-defined DBL_EPSILON:                 %g\n", DBL_EPSILON);
	double xd = 1;
	while(1+xd != 1) {
		xd = xd/2;
	}
	printf("Calculated DBL_EPSILON using while:    %g\n", xd*2);
	xd = 1;
	for(xd=1; 1+xd != 1; xd/=2){}
	printf("Calculated DBL_EPSILON using for:      %g\n", xd*2);
	xd = 1;
	do {
		xd = xd/2;
	} while( 1+xd != 1);
	printf("Calculated DBL_EPSILON using do-while: %g\n", xd*2);


	printf("C-defined LDBL_EPSILON:                 %Lg\n", LDBL_EPSILON);
	long double xld = 1;
	while(1+xld != 1) {
		xld = xld/2;
	}
	printf("Calculated LDBL_EPSILON using while:    %Lg\n", xld*2);
	xld = 1;
	for(xld=1; 1+xld != 1; xld/=2){}
	printf("Calculated LDBL_EPSILON using for:      %Lg\n", xld*2);
	xld = 1;
	do {
		xld = xld/2;
	} while( 1+xld != 1);
	printf("Calculated LDBL_EPSILON using do-while: %Lg\n", xld*2);

	return 0;
}