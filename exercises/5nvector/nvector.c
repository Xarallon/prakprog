#include<stdio.h>
#include"nvector.h"
#include<math.h>
#include<stdlib.h>

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free(v->data); free(v);} /* v->data is identical to (*v).data */

void nvector_set(nvector* v, int i, double value){ (*v).data[i]=value; }

double nvector_get(nvector* v, int i){return (*v).data[i]; }

void nvector_add(nvector* a, nvector* b) {
	for (int i = 0; i < a->size; ++i) {
		double x = nvector_get(a, i) + nvector_get(b, i);
		nvector_set(a,i,x);
	}
}

void nvector_print(char* s, nvector* v){
	printf ("%s (", s);
	int n = v->size;
	for (int i = 0; i < n-1; ++i) {
		printf("%g, ", v->data[i]);
	}
	printf("%g)\n", v->data[n]);
}

int double_equal(double a, double b) {
	double tau = 1e-6, eps = 1e-6;
	if (fabs(a - b) < tau) {
		return 1;
	}
	if (fabs(a - b) / (fabs(a) + fabs(b)) < eps / 2){
		return 1;
	}

	return 0;
}

int nvector_equal(nvector* a, nvector* b) {
	int equalSize = a->size == b->size;
	if (!equalSize) {
		return 0;
	}

	for (int i = 0; i < a->size; ++i) {
		int indexEqual = double_equal(a->data[i],b->data[i]);
		if (!indexEqual) {
			return 0;
		}
	}

	return 1;
}