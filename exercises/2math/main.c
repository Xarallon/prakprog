#include<tgmath.h>
#include<stdio.h>
#include<stdlib.h>
#include<complex.h>
int main(){
	double x = tgamma(5);
	printf("Gamma(5) = %g\n", x);

	double y = j1(0.5);
	printf("Bessel function of 0.5 is %g\n", y);

	double sr = sqrt(2);
	printf("Square root of two is %g\n", sr);

	double complex ei = exp(I) ;
	double eir = creal(ei);
	double eii = cimag(ei);
	printf("Re(e^i) = %g and Im(e^i) = %g\n", eir, eii);
	
	double complex eipi = exp(M_PI * I);
	double eipir = creal(eipi);
	double eipii = cimag(eipi); // Almost zero 
	printf("Re(e^(i*Pi)) = %g and Im(e^(i*Pi)) = %g\n", eipir, eipii);

	double complex ie = pow(I,M_E);
	double ier = creal(ie);
	double iei = cimag(ie);
	printf("Re(i^e) = %g and Im(i^e) = %g\n", ier, iei);

	float  zf =  0.1111111111111111111111111111f;
	double zd =  0.1111111111111111111111111111;
	double zl =  0.1111111111111111111111111111L;

	printf("0.1111111111111111111111111111 in float becomes\n");
	printf("%.25g\n", zf);
	printf("0.1111111111111111111111111111 in double becomes\n");
	printf("%.25lg\n", zd);
	printf("0.1111111111111111111111111111 in long double becomes\n");
	printf("%.25Lg\n", zl);

	return 0;
}